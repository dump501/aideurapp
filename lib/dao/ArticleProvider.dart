import 'package:aideur/entity/Article.dart';
import 'package:flutter/cupertino.dart';

class ArticleProvider extends ChangeNotifier{
  List<Article> articles = [];

  ArticleProvider()
  {
    articles = [];
  }

  void setArticle(List<Article> articles)
  {
    this.articles = articles;
  }

  void addArticle(Article article)
  {
    articles.add(article);
    notifyListeners();
  }

  void removeArticle(String nom)
  {
    articles.removeWhere((article) => article.nom == nom);
    notifyListeners();
  }
}