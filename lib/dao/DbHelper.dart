import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io' as io;
import 'dart:async';

import 'package:aideur/entity/Article.dart';

class DbHelper
{
  /// article table ================================================//////
  static Database _db;
  static const String COLUMN_ID = 'id';
  static const String COLUMN_NAME = 'nom';
  static const String COLUMN_PRIX = 'prix';
  static const String TABLE_ARTICLE = "articles";

  /// Database information ===================================//////
  static const String DB_NAME = 'article.db';

  Future<Database> get db async
  {
    if(_db !=null)
    {
      return _db;
    }
    _db= await initDb();
    return _db;
  }

  initDb() async
  {
    io.Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, DB_NAME);
    //var db = await openDatabase(path, version: 1, onCreate: this._onCreate);
    return await  openDatabase(path, onCreate: (db, version) => _onCreate(db, version), version: 1);
  }

  _onCreate(Database db, int version) async
  {
    await db.execute("""CREATE TABLE $TABLE_ARTICLE ($COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, $COLUMN_NAME TEXT, $COLUMN_PRIX INTEGER)""");
  }

  /// methode pour les  articles =================================================================================== ////

  // enregistrer un article
  Future<int> save(Article article) async
  {
    var dbClient = await db;
    int id = await dbClient.insert(TABLE_ARTICLE, article.toMap());
    //print("============================================== $id =============");
    return id;
  }

  // recuperer la liste de compteur
  Future<List<Article>> getArticles() async
  {
    var dbClient = await db;

    List<Map> maps = await dbClient.query(TABLE_ARTICLE, columns: [COLUMN_ID, COLUMN_NAME, COLUMN_PRIX]);
    List<Article> articles = [];
    if(maps.length >0)
    {
      for(int i=0; i<maps.length; i++)
      {
        articles.add(Article.fromMap(maps[i]));
      }
    }
    //print("=================================================================== ${articles[0].nom}");
    return articles;
  }

  //rechercher un article à partir du nom
  Future<Article> searchArticle(String name) async
  {
    var dbClient = await db;

    List<Map> maps = await dbClient.query(TABLE_ARTICLE, where: "$COLUMN_NAME = ?", whereArgs: [name]);
    return Article.fromMap(maps[0]);
  }

  //supprimer un article
Future<int> deleteArticle(String nom) async
{
  var dbClient = await db;

  return await dbClient.delete(TABLE_ARTICLE, where: "$COLUMN_NAME = ?", whereArgs: [nom]);
}

}