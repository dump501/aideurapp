import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'screen/WHomePage.dart';
import 'screen/WAjoutArticle.dart';
import 'dao/ArticleProvider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aideur',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Aideur Calcul'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final naviguatorKey = GlobalKey<NavigatorState>();
  int selectedIndex = 0;


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ArticleProvider>(
      create: (context) => ArticleProvider(),
      child: Scaffold(
        body: Navigator(
          key: naviguatorKey,
          initialRoute: '/',
          onGenerateRoute: (RouteSettings settings){
            WidgetBuilder builder;
            switch(settings.name){
              case '/':
                builder = (BuildContext context) => WHomePage();
                break;
              case '/ajoutArticle':
                builder = (BuildContext context) => WAjoutArticle();
                break;
              default:
                throw Exception("Invalid route: ${settings.name}");
            }
            return MaterialPageRoute(
              builder: builder,
              settings: settings
            );
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const<BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.exposure,
                  size: 40,
                ),
                title: Text(
                  "Calculs",
                  style: TextStyle(
                      fontWeight: FontWeight.bold
                  ),
                )
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.assignment,
                  size: 40,
                ),
                title: Text(
                  "Articles",
                  style: TextStyle(
                      fontWeight: FontWeight.bold
                  ),
                )
            ),
          ],
          backgroundColor: Colors.white,
          fixedColor: Colors.green,
          unselectedItemColor: Colors.grey,
          currentIndex: selectedIndex,
          elevation: 10,
          onTap: onItemTapped,
        ),
      ),
    );
  }

  changeDisplayedScreen(int index)
  {
    switch(index)
    {
      case 0:
        naviguatorKey.currentState.pushNamed('/');
        break;
      case 1:
        naviguatorKey.currentState.pushNamed('/ajoutArticle');
        break;
    }
  }

  onItemTapped(index)
  {
    setState(() {
      selectedIndex = index;
      changeDisplayedScreen(index);
    });
  }
  /*bottomNavigationBar()
  {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: const<BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(
                Icons.exposure,
              size: 40,
            ),
            title: Text(
                "Calculs",
              style: TextStyle(
                  fontWeight: FontWeight.bold
              ),
            )
        ),
        BottomNavigationBarItem(
            icon: Icon(
                Icons.assignment,
              size: 40,
            ),
            title: Text(
                "Articles",
              style: TextStyle(
                fontWeight: FontWeight.bold
              ),
            )
        ),
      ],
      backgroundColor: Colors.green[50],
      fixedColor: Colors.green,
      unselectedItemColor: Colors.grey,
      currentIndex: selectedIndex,
      elevation: 10,
      onTap: onItemTapped,
    );
  }*/
}
