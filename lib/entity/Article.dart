
class Article
{
  int id;
  String nom;
  int prix;

  Article({this.nom, this.prix});

  Map<String, dynamic> toMap()
  {
    var map = <String, dynamic>{
      "nom": nom,
      "prix": prix
    };
    return map;
  }

  Article.fromMap(Map<String, dynamic> map)
  {
    this.nom = map["nom"];
    this.prix = map["prix"];
  }
}