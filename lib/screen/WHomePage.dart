import 'package:aideur/dao/ArticleProvider.dart';
import 'package:aideur/dao/DbHelper.dart';
import 'package:aideur/entity/Article.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WHomePage extends StatefulWidget {
  @override
  _WHomePageState createState() => _WHomePageState();
}

class _WHomePageState extends State<WHomePage> {

  String selectedArticle;
  final formKey = GlobalKey<FormState>();
  final prixController = TextEditingController();
  List<CalculItem> calculList;

  Future<List<Article>> dbArticles;
  var dbHelper;
  List<Article> articles;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    dbHelper = DbHelper();

    dbArticles = dbHelper.getArticles();

    calculList =[];
  }

  refreshCalculList()
  {
    setState(() {
      calculList =[];
      prixController.text = "";
    });
  }

  Article searchArticle(String nom)
  {
    for(int i=0; i<articles.length; i++)
      {
        if(articles[i].nom == nom)
          {
            return articles[i];
          }
      }
    return null;
  }

  addCalculItem(CalculItem calculItem)
  {
    setState(() {
      calculList.add(calculItem);
      prixController.text = "";
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              "Calculs",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30
              ),
            ),
            RaisedButton.icon(
              onPressed: (){
                if(calculList.length == 0)
                  {

                  }
                else
                  {
                    int sum=0;
                    for(int j=0; j<calculList.length; j++)
                    {
                      sum = sum + (calculList[j].article.prix * calculList[j].quantite);
                    }
                    showCalculDialog(context, sum);
                  }

                //afficher le popup

              },
              label: Text(
                  "calculer",
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              icon: Icon(
                  Icons.add,
                color: Colors.white,
              ),
              color: Colors.red,
            )
          ],
        ),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: dbArticles,
        builder: (context, snapshot){
          if(snapshot.hasData)
            {
              if(null == snapshot.data || snapshot.data.length == 0)
                {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Veillez Enregistrer au moins un article !!",
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.green
                        ),
                      )
                    ],
                  );
                }
              Provider.of<ArticleProvider>(context).setArticle(snapshot.data);
              articles = Provider.of<ArticleProvider>(context).articles;
              return calculForm();
            }
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator()
            ],
          );
        },
      ),
    );
  }

  calculForm()
  {
    return Form(
      key: formKey,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  "Article",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22
                  ),
                ),
                DropdownButton(
                  hint: Text(
                      "Selectionnez l'article"
                  ),
                  value: selectedArticle,
                  onChanged: (newValue){
                    setState(() {
                      selectedArticle = newValue;
                    });
                  },
                  items: Provider.of<ArticleProvider>(context).articles.map((Article article){
                    return DropdownMenuItem(
                      child: FittedBox(
                        child: Text(article.nom),
                        fit: BoxFit.contain,
                      ),
                      value: article.nom,
                    );
                  }).toList(),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                validator: (value){
                  if(value.isEmpty)
                  {
                    return "Veillez entrer la quantité";
                  }
                  return null;
                },
                controller: prixController,
                decoration: InputDecoration(
                  labelText: "quantité",
                ),
                style: TextStyle(
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
                cursorColor: Colors.green,
                keyboardType: TextInputType.numberWithOptions(signed: false, decimal: false),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                RaisedButton.icon(
                  onPressed: (){
                    try{
                      if(formKey.currentState.validate())
                        {
                          int quantite = int.parse(prixController.text);
                          //recuperer l'article selectionner

                          //créer un calcul item
                          CalculItem calculItem = CalculItem(article: searchArticle(selectedArticle), quantite: quantite);

                          //inserer dans la list
                          addCalculItem(calculItem);
                        }
                    }
                    catch(e)
                    {
                      print(e);
                    }
                  },
                  label: Text(
                    "Ajouter",
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  icon: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  color: Colors.green,
                ),
                RaisedButton.icon(
                  onPressed: (){
                    refreshCalculList();
                  },
                  label: Text(
                      "Effacer",
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  icon: Icon(
                      Icons.close,
                    color: Colors.white,
                  ),
                  color: Colors.purple,
                ),
              ],
            ),
            Column(
              children: calculList.map((c) => calculListItem(c)).toList(),
            )
          ],
        ),
      ),
    );
  }

  Widget calculListItem(CalculItem calculItem)
  {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Text("${calculItem.quantite}"),
        Text("${calculItem.article.nom}")
      ],
    );
  }
}

class CalculItem{
  Article article;
  int quantite;

  CalculItem({this.article, this.quantite});
}

showAlertDialog(BuildContext context)
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    title: Text(
      "Erreur",
      style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.red
      ),
    ),
    content: Text(
        "une erreur est survenue \n verifier que le champ quantité contient un nombre \n ou contactez le dev"
    ),
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}

showCalculDialog(BuildContext context, int sum)
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    content: Text(
        "La somme est: \n $sum FCFA",
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 25,
        color: Colors.red
      ),
    ),
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}
