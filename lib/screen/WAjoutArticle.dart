import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:aideur/screen/WArticleDetail.dart';
import 'package:aideur/screen/WAddArticle.dart';
import 'package:aideur/dao/DbHelper.dart';
import 'package:aideur/entity/Article.dart';
import 'package:provider/provider.dart';
import 'package:aideur/dao/ArticleProvider.dart';

class WAjoutArticle extends StatefulWidget {
  @override
  _WAjoutArticleState createState() => _WAjoutArticleState();
}

class _WAjoutArticleState extends State<WAjoutArticle> {

  var dbHelper;

  //List<Article> articles;

  Future<List<Article>> dbArticles;

  ArticleProvider articleProvider;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.dbHelper = DbHelper();

    dbArticles = dbHelper.getArticles();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Liste d'article",
              style: TextStyle(
                color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30
              ),
            ),
          ],
        ),
      ),
      body: FutureBuilder(
        future: dbArticles,
        builder: (context, snapshot){
          if(snapshot.hasData)
          {
            if(null == snapshot.data || snapshot.data.length == 0)
            {
              return Text(
                  "pas d'article"
              );
            }
            //this.articles = snapshot.data;
            Provider.of<ArticleProvider>(context, listen: false).setArticle(snapshot.data);
            articleProvider = Provider.of<ArticleProvider>(context);
            return articleList();
          }
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator()
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          showAddDialog();
        },
        child: Icon(
            Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }


SingleChildScrollView articleList()
{
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        SizedBox(height: 30,),
        Column(
          children: Provider.of<ArticleProvider>(context).articles.map((article) => articleItem(article)).toList(),
        ),

      ],
    ),
  );
}

Widget articleItem(Article article)
{
  return Card(
    color: Colors.white,
    child: new InkWell(
      onTap: (){
        //print("tapped");
        showDetailDialog(article.nom);
        //Navigator.push(context, MaterialPageRoute(builder: (context) => WArticleDetail()));
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 40, width: 10,),
          Icon(
            Icons.apps,
            color: Colors.green,
            size: 40,
          ),
          SizedBox(height: 40, width: 10,),
          Text(
            "${article.nom}",
            style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 20
            ),
          ),
        ],
      ),
    ),
  );
}

showAddDialog()
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    content: WAddArticle(articleProvider: this.articleProvider,)
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}


showDetailDialog(String nom)
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    content: WArticleDetail(nom: nom, articleProvider: this.articleProvider,),
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}

showAlertDialog()
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    title: Text(
      "Erreur",
      style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.red
      ),
    ),
    content: Text(
        "une erreur est survenue \n verifier que le champ index contient un nombre !!"
    ),
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}
}
