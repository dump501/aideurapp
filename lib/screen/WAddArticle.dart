import 'package:aideur/dao/ArticleProvider.dart';
import 'package:aideur/entity/Article.dart';
import 'package:flutter/material.dart';

import 'package:aideur/dao/DbHelper.dart';
import 'package:provider/provider.dart';

class WAddArticle extends StatefulWidget {
  ArticleProvider articleProvider;

  WAddArticle({this.articleProvider});

  @override
  _WAddArticleState createState() => _WAddArticleState();
}

class _WAddArticleState extends State<WAddArticle> {

  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final prixController = TextEditingController();

  var dbHelper;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    dbHelper = DbHelper();

    clearTexfield();
  }

  clearTexfield()
  {
    nameController.clear();
    prixController.clear();
  }

  @override
  Widget build(context) {
    return SingleChildScrollView(
      child: form(),
    );
  }

  textField(String hint, controller)
  {
    return TextFormField(
      validator: (value){
        if(value.isEmpty)
        {
          return "Veillez remplir ce champ";
        }
        return null;
      },
      controller: controller,
      decoration: InputDecoration(
        labelText: hint,
      ),
      style: TextStyle(
        fontSize: 20,
      ),
      textAlign: TextAlign.center,
      cursorColor: Colors.green,
    );
  }


  form()
  {
    return Form(
      key: formKey,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(
              "Nouvel article",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold
              )
            ),
            textField("nom article", nameController),
            textField("prix", prixController),
            SizedBox(height: 10,),
            RaisedButton.icon(
              onPressed: () async {
                  print("======================================================== clicked");
                try{
                  if(formKey.currentState.validate())
                  {
                    int prix = int.parse(prixController.text);
                    String nom = nameController.text;

                    Article article = Article(nom: nom, prix: prix);

                    dbHelper.save(article);

                    this.widget.articleProvider.addArticle(article);

                    Navigator.of(context).pop();

                    showConfirmDialog(context);
                  }
                }
                catch(e)
                {
                  print(e);
                  showAlertDialog(context);
                }
              },
              icon: Icon(Icons.done_outline, color: Colors.white,),
              label: Text(
                "Créer",
                style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 2,
                    fontSize: 20
                ),
              ),
              color: Colors.green,
              elevation: 10,
            ),
          ],
        ),
      ),
    );
  }
}

showAlertDialog(BuildContext context)
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    title: Text(
      "Erreur",
      style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.red
      ),
    ),
    content: Text(
        "une erreur est survenue \n verifier que le champ index contient un nombre !!"
    ),
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}

showConfirmDialog(BuildContext context)
{
  //set up the button
  Widget okButton = FlatButton(
    child: Text('ok'),
    onPressed: (){},
  );

  //set the alert dialog
  AlertDialog alert = AlertDialog(
    content: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.check_circle_outline,
          color: Colors.green,
          size: 100,
        ),
        Text(
          "Article créer avec succès !!",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.green
          ),
        )
      ],
    )
  );

  //show the dialog
  showDialog(
      context: context,
      builder: (BuildContext context){
        return alert;
      }
  );
}
