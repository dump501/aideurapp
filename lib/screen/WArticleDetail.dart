import 'package:aideur/dao/ArticleProvider.dart';
import 'package:aideur/dao/DbHelper.dart';
import 'package:aideur/entity/Article.dart';
import 'package:flutter/material.dart';

class WArticleDetail extends StatefulWidget {

  String nom;
  ArticleProvider articleProvider;

  WArticleDetail({this.nom, this.articleProvider});

  @override
  _WArticleDetailState createState() => _WArticleDetailState();
}

class _WArticleDetailState extends State<WArticleDetail> {

  var dbHelper;
  Future<Article> article;

  @override
  void initState() {
    // TODO: implement initState
    dbHelper = DbHelper();
    article = dbHelper.searchArticle(widget.nom);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: article,
      builder: (context, snapshot){
        if(snapshot.hasData)
          {
            if(null == snapshot.data)
              {
                return Text("Désolé Une erreur est survenue \n veillez contacter le developpeur \n tel: 698406818 ");
              }
            return articleDetail(snapshot.data);
          }
        return Row(
          children: <Widget>[
            CircularProgressIndicator()
          ],
        );
      },
    );
  }

  articleDetail(Article article)
  {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Detail de l'article",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25
              ),
            ),
          ],
        ),
        SizedBox(height: 20,),
        Row(
          children: <Widget>[
            Text(
              "Nom: ",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20
              ),
            ),
            Text("${article.nom}",
              style: TextStyle(
                  fontSize: 20
              ),
            ),
          ],
        ),
        SizedBox(height: 10,),
        Row(
          children: <Widget>[
            Text(
              "Prix: ",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20
              ),
            ),
            Text("${article.prix} FCFA",
              style: TextStyle(
                  fontSize: 20
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FlatButton(
              onPressed: (){
                Navigator.of(context).pop();
              },
              child: Text(
                "fermer",
                style: TextStyle(
                    color: Colors.green,
                ),
              ),
            ),
            FlatButton(
              onPressed: (){
                showDeleteDialog();
              },
              child: Text(
                "supprimer",
                style: TextStyle(
                    color: Colors.red,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  showDeleteDialog()
  {
    //set up the button
    Widget okButton = FlatButton(
      child: Text('ok'),
      onPressed: (){},
    );

    //set the alert dialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Supprimer ??",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
            color: Colors.red
        ),
      ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Voulez vous supprimer l'article ${this.widget.nom} ?",
              style: TextStyle(
                color: Colors.red
              ),
            ),
            Row(
              children: <Widget>[
                FlatButton(
                  onPressed: () async{
                    dbHelper.deleteArticle(widget.nom);
                    //Article article = await dbHelper.searchArticle(this.widget.nom);
                    this.widget.articleProvider.removeArticle(widget.nom);
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "Oui",
                    style: TextStyle(
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "Non",
                    style: TextStyle(
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ],
            )
          ],
        )
    );

    //show the dialog
    showDialog(
        context: context,
        builder: (BuildContext context){
          return alert;
        }
    );
  }

}
